const Discord = require('discordie')

class CommandPromise {
  constructor (obj) {
    this.promise = Promise.resolve(obj)
  }

  then (handler) {
    this.promise = this.promise.then(handler)
    return this
  }

  catch (handler) {
    this.promise = this.promise.catch(handler)
    return this
  }
}

class Bot {
  constructor (config) {
    this.middleware = []
    this.commands = []
    this.client = null

    // Configure bot
    this.set(config)

    // Add help command
    this.on('help', `Returns commands and command information`, event => this.onHelp(event))

    // Setup command middleware
    this.use(event => {
      this.log('Middleware (auto_delete_messages)')
      this.autoDeleteMessage(event)
      return event
    })
  }

  log (message) {
    return console.log('[Jangle]', message)
  }

  use (callable) {
    this.middleware.push(callable)
    return this
  }

  connect (token) {
    this.client = new Discord({
      autoReconnect: this.config.auto_reconnect
    })

    this.dispatch = this.client.Dispatcher
    this.dispatch.on('MESSAGE_CREATE', (event) => this.onMessage(event))

    return new Promise((resolve, reject) => {
      this.client.connect({
        token: token || this.config.token
      })

      this.dispatch.on('DISCONNECTED', event => {
        this.log(`Disconnected due to ${event.error}`)
        return reject(event)
      })

      this.dispatch.on('GATEWAY_READY', event => {
        this.log(`Connected as: ${this.client.User.username}`)
        return resolve()
      })
    })
  }

  set (name, value) {
    if (typeof name === 'object') {
      this.config = name
    } else {
      this.config[name] = value
    }

    this.log(`Configuration set (${JSON.stringify(this.config)})`)
  }

  on (name, description, method) {
    this.commands.push({
      name: name,
      method: method,
      description: description
    })
  }

  onHelp (event) {
    let commands = this.commands.map(command => this.config.command_prefix + command.name).join(', ')
    event.reply(`\`\`\`${commands}\`\`\``)
  }

  onMessage (e) {
    let event = this.processMessageEvent(e)
    let promise = new CommandPromise(event)

    promise.then(event => {
      this.log(`@${event.author.username} (${event.resolveContent()})`)
      return event
    })

    if (event.isServer || (event.isBot && !event.isSelf)) {
      return
    }

    if (event.isSelf) {
      return this.autoDeleteMessage(event)
    }

    if (event.isPrivate && !event.isHelpRequest) {
      promise.then((event) => Bot.onUnauthorizedRequest(event))
      return this.addErrorHandler(promise, event)
    }

    if (event.command && event.isCommandRequest) {
      this.middleware.forEach(middleware => promise.then((event) => middleware(event)))
      promise.then((event) => event.command.method(event))
      return this.addErrorHandler(promise, event)
    }

    if (event.isCommandRequest) {
      this.middleware.forEach(middleware => promise.then((event) => middleware(event)))
      promise.then((event) => Bot.onCommandNotFound(event))
      return this.addErrorHandler(promise, event)
    }
  }

  addErrorHandler (promise, event) {
    return promise.catch(error => {
      this.log(`Error ${error.message}`)
      event.reply(error.message)
    })
  }

  processMessageEvent (e) {
    let event = {}

    // Copy Attributes
    for (var attr in e.message) event[attr] = e.message[attr]

    // Custom Attributes
    event.commandName = event.content.split(' ')[0].replace(this.config.command_prefix, '')
    event.command = this.commands.find(command => command.name === event.commandName)
    event.parts = event.content.split(' ').splice(0, 1)
    event.isSelf = event.author.id === this.client.User.id
    event.isBot = event.author.bot
    event.isHelpRequest = event.commandName.toLowerCase() === 'help'
    event.isCommandRequest = event.content[0] === this.config.command_prefix

    // Copy Commands
    event.resolveContent = e.message.resolveContent.bind(e.message)
    event.edit = e.message.edit.bind(e.message)
    event.delete = e.message.delete.bind(e.message)
    event.pin = e.message.pin.bind(e.message)
    event.unpin = e.message.unpin.bind(e.message)
    event.fetchReactions = e.message.fetchReactions.bind(e.message)
    event.addReaction = e.message.addReaction.bind(e.message)
    event.removeReaction = e.message.removeReaction.bind(e.message)
    event.clearReactions = e.message.clearReactions.bind(e.message)
    event.reply = e.message.reply.bind(e.message)

    return event
  }

  autoDeleteMessage (event) {
    if (!this.config.auto_delete_messages) return
    setTimeout(() => event.delete(), typeof this.config.auto_delete_messages === 'number'
      ? this.config.auto_delete_messages
      : 5000)
  }

  static onUnauthorizedRequest () {
    throw new Error('You are not allowed to do that.')
  }

  static onCommandNotFound () {
    throw new Error('Command not found')
  }
}

Bot.Player = require('./player')

module.exports = Bot
