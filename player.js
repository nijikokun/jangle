const Downloader = require('./downloader')
const Playlist = require('./playlist')

class Player {
  constructor () {
    this.playlist = new Playlist()
  }
}

module.exports = Player
