# Jangle

The Discord Bot Framework based on Express.js

# Documentation

```js
const Jangle = require(`jangle`)
const Bot = new Jangle([Configuration Object])

Bot.on(`ping`, `Ping pong version of Marco polo`, (event) => {
  event.reply(`pong`)
})

Bot.connect()
```

## Configuration Object

```js
{
  // Discord Bot Token
  token: 'REPLACE_ME',

  // Discord Owner ID
  owner_id: 'REPLACE_ME',

  // Command Prefix
  // Example: !help
  command_prefix: '!',

  // Default playback volume
  // Value Between 0 and 1
  volume: 0.5,

  // Auto pause when no users are in a channel
  auto_pause: true,

  // Auto play music from playlist
  //
  // Value must be absolute path to file containing list of songs
  auto_playlist: true,

  // Automatically reconnect on disconnect
  auto_reconnect: true,

  // Automatically delete messages
  //
  // Value can be either Boolean or Number for the delta
  // of delay between message sent and deleted
  auto_delete_messages: true
}
```

## Event Object

See [discordie IMessage](http://qeled.github.io/discordie/#/docs/IMessage?_k=0zu18c)

Additional `event` properties:

- `.isCommandRequest` - Determine whether message is a command request
- `.isHelpRequest` - Was message the help command
- `.isSelf` - Was message sent by the bot
- `.isBot` - Was message sent by a bot
- `.commandName` - Parsed command name without prefix
- `.command` - Command object
  - `.name` - Command name
  - `.description` - Command description
  - `.method` - Command callable function
- `.parts` - Message split by spaces with command removed

## Static Methods

- `.onUnauthorizedRequest()` - Throws an error containing basic unauthorized request message.
- `.onCommandNotFound()` - Throws an error containing basic command not found request message. 

## Instance Methods

- `.set(config)` - Set entire configuration object
- `.set(key, value)` - Update configuration object value
- `.use(event => { return event })` - Middleware to be ran before commands are ran, *must return event object*
- `.on(name, description, event => {})` - Command handler, see Event object above.
- `.log(message)` - Log message to terminal for debugging purposes
- `.connect([token])` - Connect to Discord server, `token` is optional and will override any passed configuration token

## Overriding Help Message

If you have noticed there is a built in `help` method that auto generates a list of commands and responds to the user, you can override this by extending the `Jangle` class and defining your own `onHelp` method.

# Coming Soon

- `new Bot.Player()` - Handle youtube, soundcloud, bandcamp, individual tracks and playlists, as well as built-in support for auto-playlists.


# License

```
MIT - Nijiko Yonskai - 2016
```

